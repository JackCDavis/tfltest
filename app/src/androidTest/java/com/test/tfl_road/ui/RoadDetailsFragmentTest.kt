package com.test.tfl_road.ui

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import com.test.tfl_road.MainActivity
import com.test.tfl_road.R
import com.test.tfl_road.utils.TestUtils.withRecyclerView
import com.test.tfl_road.utils.ToastMatcher
import com.test.tfl_road.viewadapter.RoadsViewHolder
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4ClassRunner::class)
class RoadDetailsFragmentTest{

    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)


    @Test
    fun clickSearchWithoutEnterText_ShouldDisplayToast(){
        val strToast:String  = "Please enter road id"
        onView(withId(R.id.btnSearch)).perform(click())
        onView(withText(strToast)).inRoot(ToastMatcher())
            .check(matches(isDisplayed()))
    }

    @Test
    fun clickSearchWithText_A20_ShouldDisplayDetails() {
        val strRoadName:String = "Road Name: A20"
        val strStatus = "Road Status: Serious"

        onView(withId(R.id.edtTerm)).perform(typeText("A20"))
        onView(withId(R.id.btnSearch)).perform(click())

        Thread.sleep(2000)
        onView(ViewMatchers.withId(R.id.recycler_view)).perform(RecyclerViewActions.scrollToPosition<RoadsViewHolder>(0))

        onView(withRecyclerView(R.id.recycler_view).atPositionOnView(0,R.id.tv_name)).check(
            matches(withText(strRoadName)))

        onView(withRecyclerView(R.id.recycler_view).atPositionOnView(0,R.id.tv_status)).check(
            matches(withText(strStatus))
        )
    }
}