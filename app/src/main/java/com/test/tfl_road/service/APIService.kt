package com.test.tfl_road.service

import com.test.tfl_road.database.DatabaseRoads
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Path

interface APIService {
    @GET("{detail}")
    fun getRoadDetails(@Path(value="detail") detail: String) : Deferred<List<DatabaseRoads>>
}