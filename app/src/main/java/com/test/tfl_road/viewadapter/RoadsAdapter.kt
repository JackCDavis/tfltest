package com.test.tfl_road.viewadapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.test.tfl_road.R
import com.test.tfl_road.database.DatabaseRoads
import com.test.tfl_road.databinding.RowRoadsBinding

class RoadsAdapter () : RecyclerView.Adapter<RoadsViewHolder>(){

    var result: List<DatabaseRoads> = emptyList()
    set(value) {
        field = value
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RoadsViewHolder {
        val withDataBinding: RowRoadsBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            RoadsViewHolder.LAYOUT,
            parent,false
        )
        return RoadsViewHolder(withDataBinding)
    }

    override fun onBindViewHolder(holder: RoadsViewHolder, position: Int) {
        holder.viewDataBinding.also {
            it.results = result[position]
        }
    }

    override fun getItemCount() = result.size

}

class RoadsViewHolder(val viewDataBinding: RowRoadsBinding) : RecyclerView.ViewHolder(viewDataBinding.root){
    companion object {
        @LayoutRes
        val LAYOUT = R.layout.row_roads
    }
}