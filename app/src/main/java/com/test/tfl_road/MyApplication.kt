package com.test.tfl_road

import android.app.Application
import com.example.movieskotlin.di.*
import com.test.tfl_road.di.databaseModule
import com.test.tfl_road.di.repositoryModule
import com.test.tfl_road.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class MyApplication: Application() {
    override fun onCreate() {
        super.onCreate()
        /**
         * Start Koin
         */
        startKoin {
            androidContext(this@MyApplication)
            androidLogger(Level.NONE)
            modules(listOf(viewModelModule, repositoryModule, netModule, apiModule, databaseModule))
        }
    }
}