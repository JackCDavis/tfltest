package com.test.tfl_road.repository

import androidx.lifecycle.LiveData
import com.test.tfl_road.database.DatabaseRoads
import com.test.tfl_road.database.RoadDatabase
import com.test.tfl_road.service.APIService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber

class RoadRepository (private val apiServices: APIService, private val database: RoadDatabase){
    suspend fun refeshRoad(word: String){
        withContext(Dispatchers.IO){
            Timber.d("Refresh road is called")
            database.roadDao.deleteAll()
            val roadList = apiServices.getRoadDetails(word).await()
            database.roadDao.insertAll(roadList)
        }
    }

    val results: LiveData<List<DatabaseRoads>> = database.roadDao.getLocalDBRoads()
}