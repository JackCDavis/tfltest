package com.test.tfl_road.database

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface RoadDao{
    @Query("select * from DatabaseRoads")
    fun getLocalDBRoads():LiveData<List<DatabaseRoads>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(roads: List<DatabaseRoads>)

    @Query("DELETE from DatabaseRoads")
    fun deleteAll()
}

@Database(entities = [DatabaseRoads::class], version = 1, exportSchema = false)
abstract class RoadDatabase: RoomDatabase(){
    abstract val roadDao :RoadDao
}