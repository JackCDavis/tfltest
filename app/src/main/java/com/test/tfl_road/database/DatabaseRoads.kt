package com.test.tfl_road.database

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class DatabaseRoads(
    @PrimaryKey
    var id: String,
    var displayName: String,
    var statusSeverity: String,
    var statusSeverityDescription: String

)