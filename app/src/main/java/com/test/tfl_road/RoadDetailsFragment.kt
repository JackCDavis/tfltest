package com.test.tfl_road

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.test.tfl_road.databinding.FragmentFirstBinding
import com.test.tfl_road.databinding.FragmentFirstBindingImpl
import com.test.tfl_road.util.LoadingState
import com.test.tfl_road.viewadapter.RoadsAdapter
import com.test.tfl_road.viewmodel.RoadDetailListViewModel
import kotlinx.android.synthetic.main.fragment_first.*
import org.koin.android.viewmodel.ext.android.viewModel

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class RoadDetailsFragment : Fragment() {

    private var viewModelAdapter: RoadsAdapter? = null
    private val viewModel by viewModel<RoadDetailListViewModel>()

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_first, container, false)
        val binding: FragmentFirstBinding = DataBindingUtil.inflate(inflater,R.layout.fragment_first,container,false)
        binding.setLifecycleOwner (viewLifecycleOwner)
        binding.viewmodel = viewModel
        viewModelAdapter = RoadsAdapter()

        binding.root.findViewById<RecyclerView>(R.id.recycler_view).apply {
            layoutManager = LinearLayoutManager(context)
            adapter = viewModelAdapter
        }

        binding.root.findViewById<Button>(R.id.btnSearch).setOnClickListener {
            viewModel.refreshDataFromRepository(edtTerm.text.toString())
            setUpObserver()
        }

        return binding.root


    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


    }

    /**
     * set the observer for definition list.
     */
    private fun setUpObserver() {
        viewModel.roadListResults.observe(viewLifecycleOwner, Observer {definition ->
            definition?.apply {
                viewModelAdapter?.result = definition
            }
        })

        viewModel.loadingState.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                LoadingState.Status.FAILED -> {
                    progressBar.visibility = View.GONE
                    Toast.makeText(activity, it.msg, Toast.LENGTH_SHORT).show()
                }
                LoadingState.Status.RUNNING -> {
                    progressBar.visibility = View.VISIBLE
                }
                LoadingState.Status.SUCCESS -> {
                    progressBar.visibility = View.GONE
                    recycler_view.visibility = View.VISIBLE

                }
            }
        })
    }
}