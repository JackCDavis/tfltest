package com.test.tfl_road.di



import com.test.tfl_road.viewmodel.RoadDetailListViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { RoadDetailListViewModel(get()) }
}
