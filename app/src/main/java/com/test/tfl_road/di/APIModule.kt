package com.example.movieskotlin.di


import com.test.tfl_road.service.APIService
import org.koin.dsl.module
import retrofit2.Retrofit

val apiModule = module {
    fun provideUserApi(retrofit: Retrofit): APIService {
        return retrofit.create(APIService::class.java)
    }

    single { provideUserApi(get()) }
}
