package com.test.tfl_road.di


import android.app.Application
import androidx.room.Room
import com.test.tfl_road.database.RoadDao
import com.test.tfl_road.database.RoadDatabase


import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

val databaseModule = module {

    fun provideDatabase(application: Application): RoadDatabase {
        return Room.databaseBuilder(application, RoadDatabase::class.java, "road")
            .fallbackToDestructiveMigration()
            .allowMainThreadQueries()
            .build()
    }


    fun provideDao(database: RoadDatabase): RoadDao {
        return database.roadDao
    }

    single { provideDatabase(androidApplication()) }
    single { provideDao(get()) }
}
