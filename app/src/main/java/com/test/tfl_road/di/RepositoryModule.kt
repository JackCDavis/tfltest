package com.test.tfl_road.di


import com.test.tfl_road.database.RoadDatabase
import com.test.tfl_road.repository.RoadRepository
import com.test.tfl_road.service.APIService
import org.koin.dsl.module

val repositoryModule = module {
    fun provideRoadRepository(api: APIService, dao: RoadDatabase): RoadRepository {
        return RoadRepository(api, dao)
    }
    single { provideRoadRepository(get(), get()) }
}