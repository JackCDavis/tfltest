package com.test.tfl_road.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.test.tfl_road.repository.RoadRepository
import com.test.tfl_road.util.LoadingState
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import retrofit2.HttpException
import java.io.IOException

class RoadDetailListViewModel(private val roadRepository: RoadRepository) : ViewModel() {
    private val _loadingState = MutableLiveData<LoadingState>()
    val loadingState: LiveData<LoadingState>
    get() =_loadingState


    val roadListResults = roadRepository.results

    private val viewModelJob = SupervisorJob()

    private val viewModelScope = CoroutineScope(viewModelJob + Dispatchers.Main)


    fun refreshDataFromRepository(word: String){
        viewModelScope.launch {
            try{
                if (word.isEmpty()){
                    _loadingState.value = LoadingState.error("Please enter road id")
                }else {
                    _loadingState.value = LoadingState.LOADING
                    roadRepository.refeshRoad(word)
                    _loadingState.value = LoadingState.LOADED
                }
            }catch (networkError: IOException){
                _loadingState.value = LoadingState.error(networkError.message)
            }catch (httpError: HttpException){
                _loadingState.value = LoadingState.error("Road id is not recognised")
               // _loadingState.value = LoadingState.error(httpError.response().toString())

            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }
}