Read Me

How to build:

To build the project go into the consts.kt file and add the API key you are going to use for the project in the blank quotation marks. Then go to the build menu and select build (or rebuild if you have already built it).

How to run:

Run the app on an emulator as follows:

In Android Studio, create an Android Virtual Device (AVD) that the emulator can use to install and run your app.
In the toolbar, select your app from the run/debug configurations drop-down menu.
From the target device drop-down menu, select the AVD that you want to run your app on.

Target device drop-down menu.

Click Run.

If you don't have any devices configured, then you need to either connect a device via USB or create an AVD to use the Android Emulator.

To find out information about the road just run the program and search the name of the road in the search box and click search. It should display the road name, the status and the status description if it is a valid road, else tell you the road is invalid so search for a valid one.

How to run tests:

To run the tests open up the com.test.tfl_road package and then open the ui package. Select the RoadDetailsFragmentTest and either click the green play at the top of the class to run all the tests. Or click the green play next to each test to run the individual test
